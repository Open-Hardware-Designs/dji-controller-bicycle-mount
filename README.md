# DJI Controller Bicycle Mount

![](V3/pictures/DSC_6347_watermark.jpg)

**What is it?**

45 Degree adjustable DJI controller mount for the bicycle handlebar.
Can be printed without any supports.

![](V3/pictures/DSC_6339_watermark.jpg)

**Why build it?**
Mounting options to buy were too expensive! And so far experience with DJI attachments were that quality is quite poor. Some designs where even 3D printed. Here are some examples [$40](V3/pictures/a_1.png),  [$50 - 3D printed](V3/pictures/a_2.png)

**Assembly**

FreeCAD files have a spreadsheet with parametric values.
Most important is to check the handlebar diameter and adjust value in the spreadsheet.

![](V3/pictures/spreadsheet.png)

**Steps**
* Insert M4 nuts into controller holder and clip pieces.
* Insert bolts into the knobs.
* (optional) I recommend using glue on bolt heads and nuts so they don't freespin if overtighened.
* Add something to prevent grip slipage. 3D printed plastic pieces can be very smooth and no matter how tight it is it can still slip. It is recommended to add a piece of rubber, electric tape or some sort of gasket to prevent clip slipping around the handlebar.
* Install the clip parts around the handle bar.
* Install controller holder and select the angle position.

![](V3/pictures/DSC_6352.jpg) ![](V3/pictures/DSC_6410.jpg) ![](V3/pictures/DSC_6411.jpg)

## V3

### Effort

CAD design (updates)  - 30min
3D printing - 6h

**Cost**
Filament - $1.5
Fasteners - $0.5

Total - $2

### Change log

* Reduced tolerances for angle adjustments to reduce movement
* Added chamfered edge for the nut so it can be printed without support
* Improved knob design so it's easier to grip
* The clamp part is round instead of just rounded corners
* Nuts on the bottom clamp part are rotated so it's edge is further from the wall

### BOM

1x Controller holder
1x HandleBar grip top
1x HandleBar grip bottom
3x Knobs (there are two different sizes but for the same M4 nut)
2x M4x20
1x M4x30

## V2

### Effort

CAD design (updates)  -
3D printing - 6h

**Cost**
Filament - $1.5
Fasteners - $0.5
Total - $2

### V2 Change Log

* Tighter fit for the Controller
* Wider grip around the handle bar
* Split the grip into two pieces so it's usable for not so flexible materials

### BOM

1x Controller holder
1x HandleBar grip top
1x HandleBar grip bottom
3x Knobs (there are two different sizes but for the same M4 nut)
2x M4x20
1x M4x30

## V1

### Requirements

Hold Controller in a stable way
Have adjustable angle
Print without supports
Use a single bolt to tighten around the handlebar
Try new knob designs

### Issues

Controller holder had 2-3 mm slack and can move.
Thin clip can split if nut is tightened too much.
Small width of the clip doesn't grip enough and it can still spin.
